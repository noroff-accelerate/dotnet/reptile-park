﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Connections.Features;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReptileParkAPI.Data;
using ReptileParkAPI.Models;
using ReptileParkAPI.Models.DTOs.Lizard;
using ReptileParkAPI.Models.DTOs.ParkRanger;

namespace ReptileParkAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LizardController : ControllerBase
    {
        private readonly ReptileParkDbContext _context;
        private readonly IMapper _mapper;

        public LizardController(ReptileParkDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;   
        }

        // GET: api/Lizard
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LizardReadDTO>>> GetLizards()
        {
            var lizards = _mapper.Map<List<LizardReadDTO>>(await _context.Lizards.Include(lz => lz.ParkRangers).ToListAsync());
            return lizards;
        }

        // GET: api/Lizard/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LizardReadDTO>> GetLizard(int id)
        {
            var lizard = await _context.Lizards.FindAsync(id);

            if (lizard == null)
            {
                return NotFound();
            }

            return _mapper.Map<LizardReadDTO>(lizard);
        }

        // PUT: api/Lizard/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLizard(int id, LizardEditDTO lizard)
        {
            if (id != lizard.Id)
            {
                return BadRequest();
            }

            var domainLizard = _mapper.Map<Lizard>(lizard);

            _context.Entry(domainLizard).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LizardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Lizard
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<LizardReadDTO>> PostLizard(LizardCreateDTO lizard)
        {
            var domainLizard = _mapper.Map<Lizard>(lizard);

            _context.Lizards.Add(domainLizard);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLizard", new { id = domainLizard.Id }, _mapper.Map<LizardReadDTO>(domainLizard));
        }

        // DELETE: api/Lizard/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLizard(int id)
        {
            var lizard = await _context.Lizards.FindAsync(id);
            if (lizard == null)
            {
                return NotFound();
            }

            _context.Lizards.Remove(lizard);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LizardExists(int id)
        {
            return _context.Lizards.Any(e => e.Id == id);
        }

    

        // assigns a ranger to a lizard using a parkRangers Id
        [HttpPut("assignRanger/{lizardId}")]
        public async Task<IActionResult> AssignRangerToLizard(int lizardId, int parkRangerId)
        {

            if (LizardExists(lizardId) == false)
            {
                return BadRequest();
            }

            // gets the lizard
            var lizard = await _context.Lizards
                .Include(l => l.ParkRangers)
                .Where(liz => liz.Id == lizardId)
                .FirstOrDefaultAsync();

            // gets the ranger
            var parkRanger = await _context.ParkRangers
                .Where(pr => pr.Id == parkRangerId)
                .FirstOrDefaultAsync();

            // adds the ranger to the lizards navigation property
            lizard.ParkRangers.Add(parkRanger);

            // updates database. FK will be assigned automatically
            await _context.SaveChangesAsync();

            return NoContent();
        }


    }
}
