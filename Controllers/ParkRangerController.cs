﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReptileParkAPI.Data;
using ReptileParkAPI.Models;
using ReptileParkAPI.Models.DTOs.Lizard;
using ReptileParkAPI.Models.DTOs.ParkRanger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Threading.Tasks;

namespace ReptileParkAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkRangerController : ControllerBase
    {
        private readonly ReptileParkDbContext _context;
        private readonly IMapper _mapper;
        public ParkRangerController(ReptileParkDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<ParkRangerReadDTO>>> GetAllRangers()
        {
            var rangers = _mapper.Map<List<ParkRangerReadDTO>>(await _context.
                ParkRangers.Include(pr=> pr.Lizards).ToListAsync());
           
            return Ok(rangers);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ParkRangerReadDTO>> GetById(int id)
        {

            var domainRanger = await _context.ParkRangers.FindAsync(id);

            if(domainRanger == null)
            {
                return NotFound();
            }

            var rangerDTO = _mapper.Map<ParkRangerReadDTO>(domainRanger);

            return Ok(rangerDTO);
        }

        [HttpPost]
        public async Task<ActionResult<ParkRangerReadDTO>> PostParkRanger([FromBody] ParkRangerCreateDTO newRanger)
        {

            var domainParkRanger = _mapper.Map<ParkRanger>(newRanger);

            _context.ParkRangers.Add(domainParkRanger);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetById", new { id = domainParkRanger.Id }, _mapper.Map<ParkRangerReadDTO>(domainParkRanger));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {
            var ranger = await _context.ParkRangers.FindAsync(id);

            if (ranger == null)
            {
                return NotFound();
            }

            _context.ParkRangers.Remove(ranger);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateRanger(int id, [FromBody]ParkRangerEditDTO updatedRangerInfo)
        {
            if(id != updatedRangerInfo.Id)
            {
                return BadRequest();
            }

           
            if (ParkRangerExists(id) == false)
            {
                return NotFound();
            }

            var domainParkRanger = _mapper.Map<ParkRanger>(updatedRangerInfo);

            _context.Entry(domainParkRanger).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            
            return NoContent();
        }

        private bool ParkRangerExists(int id)
        {
            return _context.ParkRangers.Any(e => e.Id == id);
        }

       

    }
    

}
