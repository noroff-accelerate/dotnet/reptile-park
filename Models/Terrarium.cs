﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace ReptileParkAPI.Models
{
    public class Terrarium
    {
        // PK
        public int Id { get; set; }

        [Required]
        public double CubicCm { get; set; }

        // Relationships
        // one to one
        public int? LizardId { get; set; }
        public Lizard Lizard { get; set; }

    }
}
