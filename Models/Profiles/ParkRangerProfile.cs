﻿using AutoMapper;
using ReptileParkAPI.Models.DTOs.ParkRanger;
using System.Linq;

namespace ReptileParkAPI.Models.Profiles
{
    public class ParkRangerProfile : Profile
    {
        public ParkRangerProfile()
        {// for the lizard icollection take the id from the lizard and put it in an array
            CreateMap<ParkRanger, ParkRangerReadDTO>()
                .ForMember(prDto => prDto.Lizards, opt =>opt
                .MapFrom(pr => pr.Lizards.Select(prObj=>prObj.Id).ToArray()));
            CreateMap<ParkRangerCreateDTO, ParkRanger>();
            CreateMap<ParkRangerEditDTO, ParkRanger>();
        }


    }
}
