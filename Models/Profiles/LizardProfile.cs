﻿using AutoMapper;
using ReptileParkAPI.Models.DTOs.Lizard;
using System.Linq;

namespace ReptileParkAPI.Models.Profiles
{
    public class LizardProfile : Profile
    {
        public LizardProfile()
        {
            CreateMap<Lizard, LizardReadDTO>()
                .ForMember(lzDto => lzDto.ParkRangers, opt => opt
                .MapFrom(lz => lz.ParkRangers.Select(prObj => prObj.Id).ToArray()));

            CreateMap<LizardCreateDTO, Lizard>();
            CreateMap<LizardEditDTO, Lizard>();
        }
        
          
    }
}
