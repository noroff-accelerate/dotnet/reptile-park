﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReptileParkAPI.Models.DTOs.Lizard
{
    public class LizardCreateDTO
    {
        [MaxLength(60)]
        [Required]
        public string Name { get; set; }
        public double Weight { get; set; }
        public double ScurrySpeed { get; set; }
    }
}
