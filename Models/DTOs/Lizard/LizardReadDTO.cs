﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReptileParkAPI.Models.DTOs.Lizard
{
    public class LizardReadDTO
    {
        // PK
        public int Id { get; set; }
        [MaxLength(60)]
        [Required]
        public string Name { get; set; }
        public double Weight { get; set; }
        public double ScurrySpeed { get; set; }

        // Relationships
        // 1 to 1
        public int? TerrariumId { get; set; }
        // many to many
        public int[] ParkRangers { get; set; }

    }
}
