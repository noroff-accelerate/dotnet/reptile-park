﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReptileParkAPI.Models.DTOs.ParkRanger
{
    public class ParkRangerEditDTO
    {
        // PK
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public double Weight { get; set; }
        [MinLength(10)]
        [MaxLength(15)]
        [Required]
        public string MembershipTag { get; set; }
        [MaxLength(100)]
        public string FavouriteReptile { get; set; }

    }
}
