﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReptileParkAPI.Models
{
    public class Lizard
    {
        // PK
        public int Id { get; set; }
        [MaxLength(60)]
        [Required]
        public string Name { get; set; }
        public double Weight { get; set; }
        public double ScurrySpeed { get; set; }

        // Relationships
        // 1 to 1
        public Terrarium Terrarium { get; set; }
        // many to many
        public ICollection<ParkRanger> ParkRangers { get; set; }
    }
}
