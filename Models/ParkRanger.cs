﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReptileParkAPI.Models
{
    public class ParkRanger
    {
        // PK
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public double Weight { get; set; }
        [MinLength(10)]
        [MaxLength(15)]
        [Required]
        public string MembershipTag { get; set; }
        [MaxLength(100)]
        public string FavouriteReptile { get; set; }

        // Relationships
        // many to many
        public ICollection<Lizard> Lizards { get; set; }

    }
}
