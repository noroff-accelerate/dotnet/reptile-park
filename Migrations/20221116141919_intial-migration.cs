﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ReptileParkAPI.Migrations
{
    public partial class intialmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Lizards",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: false),
                    Weight = table.Column<double>(type: "float", nullable: false),
                    ScurrySpeed = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lizards", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ParkRangers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Weight = table.Column<double>(type: "float", nullable: false),
                    MembershipTag = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    FavouriteReptile = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParkRangers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Terraria",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CubicCm = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    LizardId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Terraria", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Terraria_Lizards_LizardId",
                        column: x => x.LizardId,
                        principalTable: "Lizards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LizardParkRanger",
                columns: table => new
                {
                    LizardsId = table.Column<int>(type: "int", nullable: false),
                    ParkRangersId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LizardParkRanger", x => new { x.LizardsId, x.ParkRangersId });
                    table.ForeignKey(
                        name: "FK_LizardParkRanger_Lizards_LizardsId",
                        column: x => x.LizardsId,
                        principalTable: "Lizards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LizardParkRanger_ParkRangers_ParkRangersId",
                        column: x => x.ParkRangersId,
                        principalTable: "ParkRangers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LizardParkRanger_ParkRangersId",
                table: "LizardParkRanger",
                column: "ParkRangersId");

            migrationBuilder.CreateIndex(
                name: "IX_Terraria_LizardId",
                table: "Terraria",
                column: "LizardId",
                unique: true,
                filter: "[LizardId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LizardParkRanger");

            migrationBuilder.DropTable(
                name: "Terraria");

            migrationBuilder.DropTable(
                name: "ParkRangers");

            migrationBuilder.DropTable(
                name: "Lizards");
        }
    }
}
