﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ReptileParkAPI.Migrations
{
    public partial class seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "CubicCm",
                table: "Terraria",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.InsertData(
                table: "Lizards",
                columns: new[] { "Id", "Name", "ScurrySpeed", "Weight" },
                values: new object[,]
                {
                    { 1, "Liz", 2.5, 5.0 },
                    { 2, "Pauline", 5.5, 8.0 },
                    { 3, "Gary", 1.8999999999999999, 3.0 }
                });

            migrationBuilder.InsertData(
                table: "ParkRangers",
                columns: new[] { "Id", "FavouriteReptile", "MembershipTag", "Name", "Weight" },
                values: new object[,]
                {
                    { 1, "Snake", "PR232twtq34tq9", "Jimbo", 86.0 },
                    { 2, "Lizard", "PR232twtq34tq3", "Billy Bob", 67.0 }
                });

            migrationBuilder.InsertData(
                table: "Terraria",
                columns: new[] { "Id", "CubicCm", "LizardId" },
                values: new object[,]
                {
                    { 1, 57.0, null },
                    { 2, 109.0, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Lizards",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Lizards",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Lizards",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ParkRangers",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ParkRangers",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Terraria",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Terraria",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.AlterColumn<decimal>(
                name: "CubicCm",
                table: "Terraria",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");
        }
    }
}
