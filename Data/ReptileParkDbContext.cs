﻿using Microsoft.EntityFrameworkCore;
using ReptileParkAPI.Models;
using System.ComponentModel;

namespace ReptileParkAPI.Data
{
    public class ReptileParkDbContext:DbContext
    {
        public ReptileParkDbContext(DbContextOptions options): base(options)
        {
           
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seeding
            modelBuilder.Entity<Lizard>().HasData(
                new Lizard
                {
                    Id = 1,
                    Name = "Liz",
                    ScurrySpeed = 2.5,
                    Weight = 5
                },
                new Lizard
                {
                    Id = 2,
                    Name = "Pauline",
                    ScurrySpeed = 5.5,
                    Weight = 8
                },
                new Lizard
                {
                    Id = 3,
                    Name = "Gary",
                    ScurrySpeed = 1.9,
                    Weight = 3
                });
            modelBuilder.Entity<ParkRanger>().HasData(
                new ParkRanger
                {
                    Id = 1,
                    FavouriteReptile = "Snake",
                    Name = "Jimbo",
                    Weight = 86,
                    MembershipTag = "PR232twtq34tq9"
                },
                 new ParkRanger
                 {
                     Id = 2,
                     FavouriteReptile = "Lizard",
                     Name = "Billy Bob",
                     Weight = 67,
                     MembershipTag = "PR232twtq34tq3"
                 }
                ); ;

            modelBuilder.Entity<Terrarium>().HasData(
                new Terrarium
                {
                    Id = 1,
                    CubicCm = 57

                },
                new Terrarium
                {
                    Id = 2,
                    CubicCm = 109
                });
        }

        public DbSet<Lizard> Lizards { get; set; }
        public DbSet<ParkRanger> ParkRangers { get; set; }
        public DbSet<Terrarium> Terraria { get; set; }

        


    }
}
