# Reptile Park API

Used to demonstrate the foundations and core processes of Web APIs built in ASP.NET

## Description

An in-depth paragraph about your project and overview of use.

## Getting Started

### Dependencies

* Describe any prerequisites, libraries, OS version, etc., needed before installing program.
* ex. Windows 10

### Installing

* How/where to download your program
* Any modifications needed to be made to files/folders

### Executing program

* How to run the program
* Step-by-step bullets
```
code blocks for commands
```

## Help

Any advise for common problems or issues.
```
command to run if program contains helper info
```

## Authors

Dean von Schoultz

## Version History

* 0.1
    * Initial Release

## License

This project is licensed under the Noroff Accelerate License - see the LICENSE.md file for details

## Acknowledgments

@deanvons